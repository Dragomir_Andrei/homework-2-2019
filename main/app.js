function addTokens(input, tokens){



    //Check if the input is of type string
    if(typeof input !=='string')
    {
        throw Error('Invalid input');
    }
    //Check if the input has at lesat 6 characters
    if(input.length<6)
    {
        throw Error('Input should have at least 6 characters');
    }

    //Check if all the elements in our array is of type string
    const areAllStrings = tokens.every(x => typeof x === 'string') 
    if(areAllStrings===false && Array.isArray(tokens))
    {
        throw Error('Invalid array format')
    }
    //If the input doesn't contain any sequence of type "...", return the input as it is
    if(input.indexOf('...')==-1) // That means that the substring does not exist. We could have used .includes instead (ES6)
    {
       return input;
    }
    else
    {
        // for(var i=0; i<tokens.length; i++)
        // {
        //     input = input.replace("...", tokens[i]);
        // }
        let i = 0;
        var output = input.replace(/\.\.\./g, () => tokens[i++]);
        return output;

    }
   
    
     

    
}

const app = {
    addTokens: addTokens
}

module.exports = app;


